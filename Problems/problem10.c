#include<stdio.h>
#include<math.h>

/* function to return whether a number is prime */
int isPrime(int n){
    int i;
    if (n==2)
        return 1;
    if (n%2==0)
        return 0;
    for (i=3;i<=sqrt(n);i+=2)
        if (n%i==0)
            return 0;
    return 1;
}


int main(void)
{
	unsigned long max = 2000000;
	unsigned long long j=0;
	for(unsigned long i=2;i<=max;++i){
		if(isPrime(i)==1){
			j+=i;
			printf("%llu\t%lu\n", j, i);
	}}
	printf("Product of integers: %llu\n", j);
}
